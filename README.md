# Kenzie Academy Coin Sale Site Django Capstone(Unnamed as of now)

A site exhibiting the django skills learned by Ethan, Nathaniel, and Brett. 
The site has two primary focuses as of writings this on 2020/1/14.

1) Keeping track of coins that users currently own in an inventory management system.
2) Offering a platform to trade coins to other users

## Getting Started

Starting the project is simple.
<br>

#### First, cd to where you wish to clone the repository.
<details open>
<summary>For more details</summary>
<br>
For unix style systems please refer to
<br>

> man cd 

<br>
alternatively, for windows systems

> https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/cd

</details>
<br>

#### Second, clone the repository with the following command.
> git clone git@gitlab.com:nate998877/django-capstone.git

Note: **git** is a perquisite for this step


upon success of the command please cd into the newly created directory django-capstone
<br>

#### Third, enter the following command to initialize the required environment to launch the project
>pipenv install

Note: **pipenv** is a perquisite for this step. 



Please refer to [Pipenv's documentation](https://pypi.org/project/pipenv/) for installation guides and extra information as pipenv installation is outside the scope of this readme<span></span>.md

#### Fourth, enter the following command to enter a shell of the initialized environment
>pipenv shell

#### Fifth, then...
>python manage<span></span>.py migrate

This initializes the database as this isn't included in the repository

#### Sixth, but wait there's more
>python manage<span></span>.py runserver

#### Finally!

If everything has gone well the server will launch and include the line 

Starting development server at **http://127.0.0.1:8000/**

ctrl+left_click to open the link and start using the server!

## Built with

* Django
* Pillow
* Dotenv

  with help from
* pep8

## License?
Bah, GPLv3 maybe? I don't really care what you do. Ask Ethan :/