from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from capstone.inventory.models import Coin


@login_required
def home(request):
    return render(request, 'home.html')

@login_required
def quarters(request):
    return render(request, 'quarter/quarters.html')

@login_required
def capped_bust(request):
    coins = Coin.objects.filter(series='capped bust')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/capped_bust.html', context)

@login_required
def draped_bust(request):
    coins = Coin.objects.filter(series='draped bust')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/draped_bust.html', context)

@login_required
def seated_liberty(request):
    coins = Coin.objects.filter(series='seated liberty')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/seated_liberty.html', context)

@login_required
def standing_liberty(request):
    coins = Coin.objects.filter(series='standing liberty')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/standing_liberty.html', context)

@login_required
def washington(request):
    coins = Coin.objects.filter(series='washington')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/washington.html', context)

@login_required
def barber(request):
    coins = Coin.objects.filter(series='barber')
    for_sale = coins.filter(for_sale=True)
    context = {"coins": for_sale}
    return render(request, 'quarter/barber.html', context)
