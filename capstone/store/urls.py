from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('store/quarters/', quarters),
    path('store/quarters/capped-bust', capped_bust),
    path('store/quarters/draped-bust', draped_bust),
    path('store/quarters/seated-liberty', seated_liberty),
    path('store/quarters/standing-liberty', standing_liberty),
    path('store/quarters/washington', washington),
    path('store/quarters/barber', barber),
]
