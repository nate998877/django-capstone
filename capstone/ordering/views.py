from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Order
from .forms import NewOrderForm
from capstone.users.views import *
from django.views.generic import TemplateView
from capstone.helper import id_generator
from django.db.models import Q
from capstone.inventory.models import Coin


@login_required
def all_orders(request):
    selling = Order.objects.filter(seller=request.user)
    buying = Order.objects.filter(buyer=request.user)
    context = {"selling": selling, "buying": buying}
    return render(request, 'orders.html', context)


@login_required
def new_order(request):
    if request.method == "POST":
        form = NewOrderForm(request.POST)

        order_num = id_generator()

        if form.is_valid():
            form = form.cleaned_data

            Order.objects.create(
                order_number=order_num,
                status="placeholder for now",
                coin="another placeholder"
            )


@login_required
def order_view(request, order_id):
    order_instance = Order.objects.get(pk=order_id)
    context = {'order': order_instance}
    return render(request, 'order.html', context)




class Profile(TemplateView):
    def get(self,request):
        html='profile.html'
        bio=request.user.bio
        pic=request.user.pic
        username=request.user.username
        date_joined=request.user.date_joined

        return render(request,html,{'username':username,'bio':bio,'date_joined':date_joined,'pic':pic})


@login_required
def add_buyer(request, coin_id):
    coin = Coin.objects.get(pk=coin_id)
    order = Order.objects.get(coin=coin_id)
    order.buyer = request.user
    order.status = 'waiting payment'
    order.save()
    return redirect('/order/{}'.format(order.id))


@login_required
def change_status(request, order_id, status):
    order = Order.objects.get(id=order_id)

    if status == 'paid':
        order.status = 'paid'
        order.save()

    elif status == 'shipped':
        order.status = 'shipped'
        order.save()

    elif status == 'fulfilled':
        order.status = 'fulfilled'
        order.coin.for_sale = False
        order.coin.owner = order.buyer
        order.coin.save()
        order.save()

    else:
        return render(request, '500.html')

    return redirect('/order/{}'.format(order.id))
