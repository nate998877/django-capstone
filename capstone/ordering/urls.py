from django.urls import path
from .views import *
from .models import *
from django.contrib import admin
from django.contrib.auth.decorators import login_required

admin.site.register(Order)

urlpatterns = [
    path('orders/', all_orders),
    path('order/<int:order_id>', order_view),
    path('order/add-buyer/<int:coin_id>', add_buyer),
    path('order/status/<int:order_id>/<str:status>', change_status),
    path('profile/', login_required(Profile.as_view()))
]
