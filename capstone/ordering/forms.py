from django import forms
from .models import Order


class NewOrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            'order_number',
            'status',
            'coin'
        ]
