from django.db import models
from capstone.inventory.models import Coin
from capstone.users.models import CoinSiteUser


class Order(models.Model):
    order_number = models.IntegerField()
    time_created = models.DateField(auto_now=True)
    status = models.CharField(max_length=100)
    buyer = models.ForeignKey(CoinSiteUser, on_delete=models.CASCADE,
                              blank=True, null=True, related_name="coin_buyer")
    seller = models.ForeignKey(CoinSiteUser, on_delete=models.CASCADE)
    coin = models.ForeignKey(Coin, on_delete=models.CASCADE)

# statuses: for sale, waiting payment, paid, shipped, fulfilled
