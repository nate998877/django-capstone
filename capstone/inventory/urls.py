from django.contrib.auth.decorators import login_required
from django.urls import path
from django.contrib import admin
from .views import *
from .models import *

admin.site.register(Coin)

urlpatterns = [
    path('inventory/', login_required(inventory_view.as_view)),
    path('inventory/coin/<int:coin_id>', coin_view),
    path('inventory/new-coin/', new_coin),
]
