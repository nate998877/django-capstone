from django.db import models
from capstone.users.models import CoinSiteUser


class Coin(models.Model):
    cond_choices = (
        ('AG', 'About Good'),
        ('G', 'Good'),
        ('VG', 'Very Good'),
        ('F', 'Fine'),
        ('VF', 'Very Fine'),
        ('EF', 'Extremley Fine'),
        ('AU', 'About Uncirculated'),
        ('MS', 'Mint State'),
    )

    composition_choices = (
        ('gold', 'Gold'),
        ('silver', 'Silver'),
        ('copper', 'Copper'),
        ('other', 'Other')
    )

    denom_choices = (
        ('.05', '1/2¢'),
        ('.01', '1¢'),
        ('.02', '2¢'),
        ('.03', '3¢'),
        ('.05', '5¢'),
        ('.10', '10¢'),
        ('.20', '20¢'),
        ('.25', '25¢'),
        ('.50', '50¢'),
        ('1', '1$')
    )

    mint_choices = (
        ('P', 'philidelphia'),
        ('D', 'Denver'),
        ('S', 'San Francisco'),
        ('CC', 'Carson City'),
        ('O', 'New Orleans'),
        ('D', 'Dahlonega'),
        ('C', 'Charlotte'),
        ('W', 'West Point'),

    )

    description = models.CharField(max_length=300)
    series = models.CharField(max_length=100)
    condition = models.CharField(max_length=100, choices=cond_choices)
    year = models.IntegerField()
    price = models.IntegerField()
    mint = models.CharField(max_length=100, choices=mint_choices)
    denomination = models.CharField(max_length=100, choices=denom_choices)
    composition = models.CharField(max_length=100, choices=composition_choices)
    unique_feature = models.CharField(max_length=300)
    sku = models.IntegerField()
    proof = models.BooleanField()
    for_sale = models.BooleanField()

    owner = models.ForeignKey(CoinSiteUser, on_delete=models.CASCADE)
    obverse_pic = models.ImageField(upload_to='coin_pic/')
    reverse_pic = models.ImageField(upload_to='coin_pic/')
