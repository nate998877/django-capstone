# Generated by Django 3.0.2 on 2020-01-15 15:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0002_remove_coin_pic'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='coin',
            name='kind',
        ),
    ]
