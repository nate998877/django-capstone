from django import forms
from .models import Coin


class NewCoinForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)
    unique_feature = forms.CharField(widget=forms.Textarea)
    obverse_pic = forms.ImageField(required=False)
    reverse_pic = forms.ImageField(required=False)

    class Meta:
        model = Coin
        fields = [
            'description',
            'unique_feature',
            'series',
            'condition',
            'year',
            'price',
            'mint',
            'denomination',
            'composition',
            'for_sale',
            'proof',
        ]
