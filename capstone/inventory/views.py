from django.shortcuts import render, redirect
from .models import Coin
from .forms import NewCoinForm
from django.views import View
from django.contrib.auth.decorators import login_required
from capstone.ordering.models import Order
from capstone.helper import id_generator



@login_required
def new_coin(request):
    if request.method == "POST":
        form = NewCoinForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            form = form.cleaned_data
            sku = id_generator()

            new_coin = Coin.objects.create(
                description=form['description'],
                series=form['series'],
                condition=form['condition'],
                year=form['year'],
                price=form['price'],
                mint=form['mint'],
                denomination=form['denomination'],
                composition=form['composition'],
                unique_feature=form['unique_feature'],
                sku=sku,
                for_sale=form['for_sale'],
                proof=form['proof'],
                owner=request.user,
                obverse_pic=request.FILES['obverse_pic'],
                reverse_pic=request.FILES['reverse_pic'],
            )
            if new_coin.for_sale:
                order_num = id_generator()
                new_order = Order.objects.create(
                    order_number=order_num,
                    coin=new_coin,
                    seller=request.user,
                    status='for sale',
                )
            return redirect('/inventory/coin/{}'.format(new_coin.id))

    else:
        form = NewCoinForm()
        context = {'form': form, 'title': 'Enter a new coin'}
        return render(request, 'new_coin.html', context)


class inventory_view(View):
    def get(self, request):
        users_coins = Coin.objects.filter(owner=request.user)
        context = {"coins": users_coins, 'title': 'Inventory'}
        return render(request, 'inventory_index.html', context)


@login_required
def coin_view(request, coin_id):
    coin = Coin.objects.filter(id=coin_id).first()
    context = {"coin": coin, 'title': 'Coin view'}
    return render(request, 'coin_view.html', context)

