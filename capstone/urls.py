from django.contrib import admin
from django.urls import path
from capstone.store.urls import urlpatterns as store_urls
from capstone.ordering.urls import urlpatterns as ordering_urls
from capstone.inventory.urls import urlpatterns as inventory_urls
from capstone.users.urls import urlpatterns as user_urls
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = []
urlpatterns += store_urls
urlpatterns += ordering_urls
urlpatterns += inventory_urls
urlpatterns += user_urls

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                                document_root=settings.MEDIA_ROOT)
