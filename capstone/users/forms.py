from .models import CoinSiteUser
from django import forms


class AddUserForm(forms.Form):
    username = forms.CharField(label='Your name', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput)
    bio = forms.CharField(widget=forms.Textarea)
    pic = forms.ImageField(required=False)
    email = forms.CharField(widget=forms.EmailInput)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput)
