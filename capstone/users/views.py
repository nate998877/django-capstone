from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.views import View
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect, reverse
from .forms import *
from .models import *


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user:= (
                    authenticate(
                        request,
                        username=data["username"],
                        password=data["password"]
                    )
                ):
                login(request, user)
                return HttpResponseRedirect(
                    request.GET.get('next', reverse('home'))
                )
        return render(request, 'login_wrapper.html', {'form': form, 'msg':"Username or password is incorrect"})
    form = LoginForm()
    return render(request, 'login_wrapper.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))



class add_user(View):
    form_class = AddUserForm
    template_name = 'new_user.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {
            'form': form,
            'html': "<a href='/login'>back</a>"
        })

    def post(self, request, *args, **kwargs):
        form = AddUserForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            data = form.cleaned_data
            user = CoinSiteUser.objects.create_user(
                username=data["username"],
                password=data["password"],
                bio=data["bio"],
                pic=request.FILES['pic'],
                email=data["email"],
            )

            login(request, user)
            return HttpResponseRedirect(
                request.GET.get('next', reverse('home'))
            )

