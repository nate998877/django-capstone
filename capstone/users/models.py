from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class CoinSiteUserManager(BaseUserManager):
    def create_user(self, email, username, password, bio, pic):
        if not email:
            raise ValueError("Users must have email")
        if not username:
            raise ValueError("Users must have username")
        if not password:
            raise ValueError("Users must have password")

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            bio=bio,
            pic=pic
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)


class CoinSiteUser(AbstractBaseUser):
    bio = models.TextField(blank=True, null=True)
    pic = models.ImageField(upload_to='profile_pic/')
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)

    # REQUIRED ↓
    username = models.CharField(
        verbose_name="username", max_length=30, unique=True)
    date_joined = models.DateTimeField(
        verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last_login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

    objects = CoinSiteUserManager()

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True
