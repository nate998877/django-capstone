from capstone.helper import id_generator

def test_helper():
    sku = id_generator()
    assert type(sku) == int
    assert sku > 10**9 and sku < 10**10-1